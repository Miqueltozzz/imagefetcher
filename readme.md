# ImageFetcher
This tool is meant to download all images from a webpage quickly.
This is achieved by executing all IO tasks (downloading, saving to disk) in threads. 
This solution works fine for typical webpages, and is not meant to be ideal for edge cases, such as 1 GB HTML page with one image.

The tool searches the downloaded html page for img tags, and tries to download the image where src-tag points to.
Base64 embedded images etc. are not saved to disk.
Images are saved to .\images subfolder, and  same image name is used as on the webpage.
Matching filenames are overwritten without warning.


Python 3.9.12 was used for development, and nothing has been tested with other versions.

All commands listed below should be run from the repository's root directory (where setup.py is located)

## Installation

To install the image fetcher:
```sh
pip install -e .
```

To install packages needed for running tests:
```sh
pip install .[test]
```

## Running 
To fetch images for a website:
```sh
python imagefetcher "https://jimms.fi"
```

Or just run the __main__.py file from your favourite IDE with the URL as a parameter.


To see supported command line parameters:
```sh
python imagefetcher.py --help
```

## Running tests
```sh
pytest tests
```
