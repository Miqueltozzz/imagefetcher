import argparse

import sys
import logging
import requests
import datetime

from imagefetcher.downloader import ThreadPoolDownloader
from imagefetcher.imageparser import ImageParser, is_page_url


log = logging.getLogger(__name__)


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("url", type=str, help="URL of the page to fetch images from")
    parser.add_argument("--timeout", type=float, required=False, default=7.0, help="how long to wait for each download")
    parser.add_argument("--threads", type=int, required=False, default=50,
                        help="how many threads to use for downloading and saving")
    args = parser.parse_args()

    if args.threads < 1:
        raise ValueError("Must use at least one thread!")

    if args.timeout <= 0.0:
        raise ValueError("Timeout must be greater than zero!")

    return args


def main() -> None:
    args = parse_args()
    if not is_page_url(args.url):
        log.error("Invalid URL: %s", args.url)
        return
    try:
        start_time = datetime.datetime.now()

        page = requests.get(args.url, timeout=args.timeout).text

        page_fetch_time = datetime.datetime.now()

        downloader = ThreadPoolDownloader(threads=args.threads, timeout=args.timeout)
        parser = ImageParser(downloader=downloader)
        parser.handle_page(page, args.url)
        parsed_time = datetime.datetime.now()
        downloader.wait()
        end_time = datetime.datetime.now()

        log.info("Page download time: %s, Parsing time: %s, Image download time: %s, total time: %s",
                 page_fetch_time-start_time,
                 parsed_time-page_fetch_time,
                 end_time-parsed_time,
                 end_time-start_time)
    except requests.exceptions.RequestException as e:
        log.error("Could not fetch contents of %s, details: %s", args.url, e)
    except Exception as e:
        log.error("Unexpected error when attempting to handle %s, details: %s", args.url, e)


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        stream=sys.stdout,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    )
    main()
