import mimetypes
import logging
from urllib.parse import urljoin, urlparse
from html.parser import HTMLParser
from imagefetcher.downloader import Downloader

log = logging.getLogger(__name__)


def is_image_url(url: str) -> bool:
    """ Does the url point to an image?
    Checks that url has at least scheme (https://), netlock (domain.fi) and path (/img/image.png)
    Not foolproof at all, but rejects some obviously non-image urls.
    """
    try:
        result = urlparse(url)
        return all([result.scheme, result.path, result.netloc])
    except ValueError:
        return False


def is_page_url(url: str) -> bool:
    """ Does to url point to a webpage?
    Check that url has at least scheme (https://) and netlock (domain.fi).
    Not foolproof at all, but rejects some obviously bad urls."""
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False


def is_mimetype_image(url: str) -> bool:
    mimetype, encoding = mimetypes.guess_type(url)
    return mimetype and mimetype.startswith('image')


class ImageParser(HTMLParser):
    """
    Class for going through HTML text, calls the downloader for everything that seems to be an image.
    """
    def __init__(self, downloader: Downloader, *args, **kwargs):
        super(ImageParser, self).__init__(*args, **kwargs)
        self.base_url = ""  # This is needed for rebuilding relative urls
        self._downloader = downloader

    def handle_starttag(self, tag, attrs) -> None:
        """
        This is called for each start tag - if the tag seems to be for an image, calls the download function.
        """
        if tag == "img":
            for attr in attrs:
                if len(attr) >= 2 and attr[0] == "src":
                    url = attr[1]
                    # Fix relative urls and links without scheme. No effect on abs urls.
                    url = urljoin(base=self.base_url, url=url)
                    if is_image_url(url) and is_mimetype_image(url):
                        log.info("Downloading %s", url)
                        self._downloader.download_and_save(url)
                    else:
                        log.debug("Rejected download with URL: %s", url)
                    break  # There cannot be multiple images within a single tag

    def error(self, message) -> None:
        log.error(message)

    def handle_page(self, text, url) -> None:
        # This should be called instead of feed, to make sure that base_url is correctly set
        self.base_url = url
        self.feed(text)
