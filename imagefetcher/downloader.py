import logging
import requests
import os
import concurrent.futures

log = logging.getLogger(__name__)


class Downloader:
    """
    Basic class for downloading images one-by-one.
    """
    def __init__(self, timeout: float = 4.0, save_path: str = "images", *args, **kwargs):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        self._save_path = save_path
        self._timeout = timeout

    def download(self, url: str) -> None:
        """
        Downloads and saves to disk the image from the url.
        """
        try:
            filename = url.split('/')[-1]
            img_data = requests.get(url, timeout=self._timeout).content
            filepath = os.path.join(self._save_path, filename)
            with open(filepath, 'wb') as handle:
                handle.write(img_data)
        except requests.exceptions.RequestException as e:
            log.error("Fetching image %s failed, details: %s", url, e)
        except OSError as e:
            log.error("Could not write file to disk, url: %s, details: %s", url,  e)
        except Exception as e:
            log.exception("Could not handle image %s, details: %s", url, e)

    def download_and_save(self, url: str) -> None:
        self.download(url)

    def wait(self) -> None:
        pass


class ThreadPoolDownloader(Downloader):
    """
    Faster version of the class, which runs the IO operations on a threadpool.
    A high thread count should be used if downloading a lot of small images.
    """
    def __init__(self, threads: int = 50, *args, **kwargs):
        super(ThreadPoolDownloader, self).__init__(*args, **kwargs)
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=threads)

    def download_and_save(self, url: str) -> None:
        self.executor.submit(self.download, url)

    def wait(self) -> None:
        # Wait for all the downloads to complete (or fail)
        self.executor.shutdown(wait=True)
