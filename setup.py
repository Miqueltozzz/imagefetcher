from setuptools import setup, find_packages

setup(
    name='imagefetcher',
    version='0.1.0',
    packages=find_packages(),
    install_requires=[
        'requests==2.27.1',
    ],
    extras_require={
        "test": ["pytest", "pytest-mock"]
    }
)
