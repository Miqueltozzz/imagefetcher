from imagefetcher.imageparser import ImageParser
from unittest.mock import MagicMock
import pytest

html_no_images = ("<!DOCTYPE html><html><body><h1>My First Heading</h1><p>My first paragraph.</p>"
                  "</body></html>")
html_one_image = ("<!DOCTYPE html><html><body><h1>My First Heading</h1><p>My first paragraph.</p> "
                  "<img src=\"img_girl.jpg\" alt=\"Girl in a jacket\">"
                  " </body></html>")


@pytest.fixture
def downloader():
    downloader = MagicMock()
    downloader.download_and_save = MagicMock()
    return downloader


def test_one_image_html(downloader):
    parser = ImageParser(downloader=downloader)
    parser.handle_page(html_one_image, url="https://fake.fi")
    assert downloader.download_and_save.call_count == 1


def test_no_image_html(downloader):
    parser = ImageParser(downloader=downloader)
    parser.handle_page(html_no_images, url="https://fake.fi")
    assert downloader.download_and_save.call_count == 0
