from imagefetcher.imageparser import is_image_url, is_page_url


def test_page_urls():
    valid_urls = [
        "http://hs.fi",
        "https://hs.fi",
        "https://hs.fi/subpage",
        "https://hs.fi/subpage#fragment"
    ]
    invalid_urls = [
        "hs.fi",
        "",
        None,
        "{",
        "abcdefg",
    ]

    for url in valid_urls:
        assert is_page_url(url) is True

    for url in invalid_urls:
        assert is_page_url(url) is False


def test_image_urls():
    valid_urls = [
        "http://hs.fi/image.png",
        "https://hs.fi/image.png",
        "https://hs.fi/subpage/image.jpg",
    ]
    invalid_urls = [
        "http://hs.fi",
        "https://hs.fi",
        "hs.fi",
        "",
        None,
        "{",
        "ajwoeijaowi",
    ]

    for url in valid_urls:
        assert is_image_url(url) is True

    for url in invalid_urls:
        assert is_image_url(url) is False
